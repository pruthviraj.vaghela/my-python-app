# app.py
def add(a, b):
    return a + b

def subtract(a, b):
    return a - b

def main():
    print("Testing simple math functions...")
    print(f"3 + 4 = {add(3, 4)}")
    print(f"5 - 2 = {subtract(5, 2)}")

if __name__ == "__main__":
    main()
